var express = require( 'express' )
  , request = require( 'request' )

module.exports = function() {
	App.express.use( express.static( App.dirs.client ) );
	App.express.use( '/common', express.static( App.dirs.common ) );
}