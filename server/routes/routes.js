
module.exports = function() {
	require( './http/static' )();

	App.io.on( 'connection', function( socket ) {
		socket.emit( 'join', { hello: 'world' } );
	});
}