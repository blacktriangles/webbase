
var env = process.env.NODE_ENV || 'development'
  , package = require( './package.json' )
  , path = require( 'path' )
  , express = require( 'express' )
  , bodyParser = require( 'body-parser' )
  , io = require( 'socket.io' )
  , http = require( 'http' )
  , rootDir = __dirname

global.App = {
	express: express()
  , passport: null
  , server: null
  , io: null
  , port: process.env.PORT || 9800
  , package: package
  , version: package.version
  , env: env
  , routes: require( './routes/routes' )
  , dirs: {
		root: rootDir
	  , client: path.join( rootDir, '../client/' )
	  , common: path.join( rootDir, '../common/' )
	}

  , start: function( addRoutes ) {
		App.loadAllCommon();
		
		App.server = http.createServer( App.express );
		App.io = io( App.server );

		App.express.use( bodyParser() );
		App.routes();
		if( addRoutes !== undefined ) {
			addRoutes();
		}

		App.loadAllCommon();

		App.server.listen( App.port );
		console.log( 'Started ' + App.package.name + ' in ' + App.env + ' mode on port ' + App.port );
	}

  , loadAllCommon: function() {
		App.constants = App.loadCommon( 'js/constants' );
	}

  , loadCommon: function( partial ) {
		return require( path.join( App.dirs.common, partial ) );
	}
}